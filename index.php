<?php

require ("animal.php");
require ("frog.php");
require ("ape.php");

$sheep = new Animal("shaun");


echo $sheep->name . "<br>"; // "shaun"
echo $sheep->legs . "<br>"; // 2
echo $sheep->cold_blooded . "<br>"; // false

echo "<br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->name . "<br>"; 
echo $sungokong->legs . "<br>";
echo $sungokong->yell() . "<br>" ;// "Auooo"

echo "<br>";

$kodok = new Frog("buduk");
echo $kodok->name . "<br>"; 
echo $kodok->legs . "<br>";
echo $kodok->jump() . "<br>" ; // "hop hop"





// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

?>